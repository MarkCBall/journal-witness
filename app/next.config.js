const withTM = require("next-transpile-modules")([
  "@journal-witness/libraries-crypto",
])

module.exports = withTM({
  env: {
    FIREBASE_CLIENT_CREDENTIALS:
      process.env.NEXT_PUBLIC_FIREBASE_CLIENT_CREDENTIALS,
  },
  async redirects() {
    return [
      {
        source: "/",
        destination: "/journal",
        permanent: false,
      },
    ]
  },
})
