export default {
  firebase: {
    clientCredentials: JSON.parse(process.env.FIREBASE_CLIENT_CREDENTIALS),
  },
  entries: {
    displayedPerPage: 10,
  },
}
