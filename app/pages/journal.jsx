import React, { useState } from "react"
import withUser from "../components/auth/with-user"
import useJournalEntries from "../components/journal/hooks/useJournalEntries"
import HeaderRow from "../components/journal/HeaderRow"
import NewRow from "../components/journal/NewRow"
import Row from "../components/journal/Row"
import {
  Table,
  Loader,
  Segment,
  Icon,
  Grid,
  Label,
  Button,
  Modal,
} from "semantic-ui-react"
import styled from "styled-components"

const Padded = styled.div`
  @media (min-width: 1000px) {
    padding-top: 10px;
    padding-left: 50px;
    padding-right: 50px;
  }
`

function Journal() {
  const [modalOpen, setModalOpen] = useState(false)
  const onOpen = () => setModalOpen(true)
  const onClose = () => setModalOpen(false)
  const {
    initialLoading,
    data: entries,
    error,
    loadNext,
    loadPrevious,
    nextLoading,
    previousLoading,
    hasNext,
    hasPrevious,
  } = useJournalEntries()
  if (initialLoading) {
    return (
      <Padded>
        <Segment>
          <Table color={"blue"} fixed>
            <HeaderRow />
            <Table.Body>
              <Table.Row />
            </Table.Body>
          </Table>
          <Loader active />
        </Segment>
      </Padded>
    )
  }
  if (error) {
    throw error
  }

  return (
    <Padded>
      <Modal
        closeIcon
        style={{ paddingBottom: "15px" }}
        content={<NewRow onClose={onClose} />}
        trigger={
          <Button size={"huge"} color={"blue"}>
            Add new entry
          </Button>
        }
        open={modalOpen}
        onClose={onClose}
        onOpen={onOpen}
      />
      <Table celled fixed color={"blue"}>
        <HeaderRow />
        <Table.Body>
          {entries?.map((entry) => (
            <Row key={entry.id} entry={entry} />
          ))}
        </Table.Body>
      </Table>
      <Grid textAlign="center" verticalAlign="top">
        <Label.Group as={"a"} size="huge">
          {hasPrevious && (
            <Label onClick={loadPrevious}>
              <Icon name={"angle left"} />
              {previousLoading && <Loader size={"mini"} inline active />}
            </Label>
          )}

          {hasNext && (
            <Label onClick={loadNext}>
              <Icon name={"angle right"} />
              {nextLoading && <Loader size={"mini"} inline active />}
            </Label>
          )}
        </Label.Group>
      </Grid>
    </Padded>
  )
}

export default withUser(Journal)
