import "semantic-ui-css/semantic.min.css"
import "react-toastify/dist/ReactToastify.css"
import ErrorBoundary from "../components/common/ErrorBoundary"
import { toast } from "react-toastify"
toast.configure()

function MyApp({ Component, pageProps }) {
  return (
    <ErrorBoundary>
      <Component {...pageProps} />
    </ErrorBoundary>
  )
}

export default MyApp
