import firebase from "firebase/app"
import "firebase/firestore"
import "firebase/auth"
import config from "../../client-config"

//Don't initialize firebase if its already initialized
!firebase.apps.length &&
  firebase.initializeApp(config.firebase.clientCredentials)

export default firebase.auth()
export const firebaseApp = firebase
