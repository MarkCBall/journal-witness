import React from "react"
import auth, { firebaseApp } from "./firebase-auth"
import NavBar from "../../components/layout/NavBar"
import SignIn from "../../components/layout/SignIn"

import { useAuthState } from "react-firebase-hooks/auth"
function withUser(WrappedComponent) {
  return function (props) {
    const [user, loading, error] = useAuthState(auth)
    if (loading) {
      return "Authenticating...."
    }
    if (error) {
      throw error
    }

    if (user) {
      return (
        <>
          <NavBar signOut={() => auth.signOut()} />
          <WrappedComponent {...props} />
        </>
      )
    }

    const signInWithGoogle = () => {
      const provider = new firebaseApp.auth.GoogleAuthProvider()
      auth.signInWithPopup(provider)
    }
    return <SignIn signIn={signInWithGoogle} />
  }
}

export default withUser
