import React, { useState, useRef, useEffect } from "react"
import { last, reverse, dropRight } from "lodash"

const flipOrder = (str) => {
  if (str === "asc") return "desc"
  if (str === "desc") return "asc"
  throw new Error("Unknown orderBy string")
}

/**
 * Fetches and throws away an extra doc on each pagination in order
 * to determine if there are more documents. This isn't perfect and
 * can lead to the first page having one document/zero documents if
 * the data being subscribed to changes.
 *
 *  * A more robust solution would be to subscribe to the max and min docs
 * and do a refactor.
 *
 * Also, currently, any time any document in the subscription changes,
 * every document is re-fetched. This can be optimized be looking at
 * docChanges and fetching accordingly.
 *
 * **/

function usePaginatedCollection(
  collection,
  fieldPath,
  { orderByDirection = "asc", limit = 25 }
) {
  const [state, setState] = useState({ initialLoading: true })
  const subscription = useRef()
  const fetchSize = limit + 1

  useEffect(() => {
    subscription.current = collection
      .orderBy(fieldPath, orderByDirection)
      .limit(fetchSize)
      .onSnapshot({
        next: (snapshot) => {
          const hasNext = snapshot.size === fetchSize
          const relevantDocs = hasNext
            ? snapshot.docs.slice(0, -1)
            : snapshot.docs
          setState({
            //note, docChanges can be used  to indicate which docs don't need to be re-fetched
            data: relevantDocs.map((docSnapshot) => docSnapshot.data()),
            hasNext,
          })
        },
        error: (error) => setState({ error }),
      })
  }, [])

  const loadNext = () => {
    subscription.current()
    setState((state) => ({ ...state, nextLoading: true }))
    subscription.current = collection
      .orderBy(fieldPath, orderByDirection)
      .limit(fetchSize)
      .startAfter(last(state.data)[fieldPath])
      .onSnapshot({
        next: (snapshot) => {
          const hasNext = snapshot.size === fetchSize
          const relevantDocs = hasNext
            ? dropRight(snapshot.docs)
            : snapshot.docs
          setState({
            data: relevantDocs.map((docSnapshot) => docSnapshot.data()),
            hasNext,
            hasPrevious: true,
          })
        },
        error: (error) => setState({ error }),
      })
  }
  const loadPrevious = () => {
    subscription.current()
    setState((state) => ({ ...state, previousLoading: true }))
    subscription.current = collection
      .orderBy(fieldPath, flipOrder(orderByDirection))
      .limit(fetchSize)
      .startAfter(state.data[0][fieldPath])
      .onSnapshot({
        next: (snapshot) => {
          const hasPrevious = snapshot.size === fetchSize
          const relevantDocs = hasPrevious
            ? dropRight(snapshot.docs)
            : snapshot.docs
          const unreversedRelevantDocs = reverse(relevantDocs)
          setState({
            data: unreversedRelevantDocs.map((docSnapshot) =>
              docSnapshot.data()
            ),
            hasNext: true,
            hasPrevious,
          })
        },
        error: (error) => setState({ error }),
      })
  }

  return {
    initialLoading: state.initialLoading,
    nextLoading: state.nextLoading,
    previousLoading: state.previousLoading,
    hasPrevious: state.hasPrevious,
    hasNext: state.hasNext,
    error: state.error,
    data: state.data,
    loadNext,
    loadPrevious,
  }
}

export default usePaginatedCollection
