import React from "react"
import { Icon, Header, Grid } from "semantic-ui-react"

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { error: false }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo,
    })
    // logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.error) {
      return (
        <Grid
          textAlign="center"
          style={{ height: "100vh" }}
          verticalAlign="middle"
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="red" textAlign="center">
              Something went wrong.
            </Header>
            <Icon name={"bug"} size={"massive"} />
            <br />
            {this.state.error.toString()}
          </Grid.Column>
        </Grid>
      )
    }
    return this.props.children
  }
}

export default ErrorBoundary
