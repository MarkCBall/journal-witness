import React from "react"
import { Button, Container, Menu, Icon, Header } from "semantic-ui-react"

function NavBar({ signOut }) {
  return (
    <>
      <Menu size="large">
        <Menu.Item position="left">
          <Icon name={"book"} size={"large"} color={"blue"} />
          <Header color="blue">Prove your journal entries</Header>
        </Menu.Item>
        <Container>
          {/*<Menu.Item as='a'>Journal</Menu.Item>*/}
          <Menu.Item position="right">
            <Button onClick={() => signOut()} as="a">
              Log out
            </Button>
          </Menu.Item>
        </Container>
      </Menu>
    </>
  )
}

export default NavBar
