import React from "react"
import {
  Button,
  Grid,
  Header,
  Message,
  Segment,
} from 'semantic-ui-react'

function SignIn ({signIn}) {
  return (
    <Grid textAlign='center' style={{ height: '100vh' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='blue' textAlign='center'>
          Prove your journal entries
        </Header>
          <Segment stacked>
            <Message>
              <Message.Header>Features</Message.Header>
              <Message.List>
                <Message.Item>Track all revisions automatically</Message.Item>
                <Message.Item>Substantiate revision date on the blockchain</Message.Item>
              </Message.List>
            </Message>
            <Header as='h2' color='blue' textAlign='center'>
              Begin now
            </Header>
            <Button color='blue' fluid size='large' onClick={signIn}>
              Login
            </Button>
          </Segment>
      </Grid.Column>
    </Grid>
  )
}

export default SignIn
