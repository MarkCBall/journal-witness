import React from "react"
import { Table } from "semantic-ui-react"

function HeaderRow() {
  return (
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell width="2" textAlign={"center"}>
          Date
        </Table.HeaderCell>
        <Table.HeaderCell width="6" textAlign={"center"}>
          First note
        </Table.HeaderCell>
        <Table.HeaderCell width="6" textAlign={"center"}>
          Second note
        </Table.HeaderCell>
        <Table.HeaderCell width="6" textAlign={"center"}>
          Third note
        </Table.HeaderCell>
      </Table.Row>
    </Table.Header>
  )
}

export default HeaderRow
