import React from "react"
import moment from "moment"
import { Table, Label } from "semantic-ui-react"
import TimeAgo from "timeago-react"
import styled from "styled-components"

const RightJustified = styled.div`
  float: right;
  font-size: smaller;
  padding-bottom: 3px;
`

function DateCell({ entry }) {
  return (
    <Table.Cell>
      {entry.updatedAt && (
        <RightJustified>
          Edited:{" "}
          <TimeAgo datetime={entry.updatedAt?.toDate()} locale="en-US" />
        </RightJustified>
      )}
      <br />
      <Label size={"small"}>
        Created:{moment(entry.createdAt?.toDate()).format("YYYY, MMM Do")}
      </Label>
    </Table.Cell>
  )
}
export default DateCell
