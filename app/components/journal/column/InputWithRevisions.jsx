import React from "react"
import TextInput from "./TextInput"
import { has } from "lodash"
import Revisions from "../revisions/Revisions"
import useWriteJournal from "../hooks/useWriteJournal"

function InputWithRevisions({ field, entry, entryRevisions }) {
  const { makeUpdateDoc } = useWriteJournal()
  const onSave = (val) => makeUpdateDoc(entry.id)({ [field]: val })

  if (entryRevisions.initialLoading) {
    return (
      <TextInput
        onSave={onSave}
        initialValue={entry[field]}
        labelComponent={null}
      />
    )
  }

  const revisionData = entryRevisions.data
    ?.filter((update) => has(update, field))
    .map((r) => ({
      updatedAt: r.updatedAt,
      witnessAt: r.witness,
      text: r[field],
      hashInputs: r.hashInputs,
      txId: r.txId,
    }))

  return (
    <TextInput
      onSave={onSave}
      initialValue={entry[field]}
      labelComponent={
        revisionData.length ? <Revisions revisionData={revisionData} /> : null
      }
    />
  )
}
export default InputWithRevisions
