import React, { useState, useEffect, useLayoutEffect, useRef } from "react"
import { Table, Input, Icon, Button, TextArea, Form } from "semantic-ui-react"
import styled from "styled-components"
import { toast } from "react-toastify"

const Outer = styled.div`
  display: flex;
`
const Left = styled.div`
  width: 100%;
`

function TextInput({ onSave, initialValue = "", labelComponent }) {
  const [content, setContent] = useState(initialValue)
  const [throwErr, setThrowErr] = useState(null)
  const [editorOpen, setEditorOpen] = useState(false)
  const inputRef = useRef(null)
  useLayoutEffect(()=>{
    //force flush async changes
    setContent(initialValue)
  },[initialValue])
  useEffect(()=>{
    if (editorOpen)
      inputRef.current.focus()
  },[editorOpen])

  const toggleEditorOpen = () => setEditorOpen((b) => !b)
  const isDirty = content !== initialValue
  const saveText = () => {
    isDirty &&
      onSave(content).catch((err) => {
        setContent(initialValue)
        setThrowErr(err)
      })
  }
  const resetText = () => setContent(initialValue)
  if (throwErr) {
    setThrowErr(() => {
      toast.error("Could not save change - reverting to previous text", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
      })
      return null
    })
  }

  if (editorOpen) {
    const onSave = () =>{
      saveText()
      toggleEditorOpen()
    }
    const onClear = () =>{
      resetText()
      toggleEditorOpen()
    }

    return (
      <Table.Cell>
        <Form>
          <TextArea
            value={content}
            ref={inputRef}
            onBlur={()=>{
              toggleEditorOpen()
            }}
            onChange={(e) => setContent(e.target.value)}
            rows={6}
          />
        </Form>
        <Button
          size={"mini"}
          onMouseDown={onSave}//registered prior to onBlur, unlike onClick
          color="blue"
          floated={"right"}
        >
          Save
        </Button>
        {isDirty && <Button
            size={"mini"}
            onMouseDown={onClear}
            floated={"right"}
          >
            Clear changes
          </Button>
        }
      </Table.Cell>
    )
  }

  return (
    <Table.Cell>
      <Outer>
        <Left>
          <Input
            value={content}
            onFocus={toggleEditorOpen}
            fluid
            size={"small"}
            label={
              !!labelComponent && {
                content: <>{labelComponent}</>,
                color: "blue",
                basic: true,
                active: false,
              }
            }
            icon={<>
              {content !== initialValue && <Icon color={"red"} name={"save outline"} />}
              </>}
          />
        </Left>
      </Outer>
    </Table.Cell>
  )
}

export default TextInput
