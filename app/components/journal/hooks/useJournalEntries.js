import React from "react"
import auth, { firebaseApp } from "../../auth/firebase-auth"
const firestore = firebaseApp.firestore()
import { useAuthState } from "react-firebase-hooks/auth"
import usePaginatedCollection from "../../common/hooks/usePaginatedCollection"
import config from "../../../client-config"

function useJournalEntries() {
  const [{ uid }] = useAuthState(auth)
  const collection = firestore.collection(`users/${uid}/entries`)
  return usePaginatedCollection(collection, "createdAt", {
    orderByDirection: "desc",
    limit: config.entries.displayedPerPage,
  })
}

export default useJournalEntries
