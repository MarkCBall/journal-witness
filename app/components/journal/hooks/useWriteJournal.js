import React from "react"
import auth, { firebaseApp } from "../../auth/firebase-auth"
const firestore = firebaseApp.firestore()
import { useAuthState } from "react-firebase-hooks/auth"

function useWriteJournal() {
  const [{ uid }] = useAuthState(auth)
  const entriesCollection = firestore.collection(`users/${uid}/entries`)
  const insertDoc = async (data) => {
    const newDocRef = entriesCollection.doc()
    const batch = firestore.batch()
    batch.set(newDocRef, {
      ...data,
      id: newDocRef.id,
      createdAt: firebaseApp.firestore.FieldValue.serverTimestamp(),
    })
    batch.set(newDocRef.collection("dataCommit").doc(), {
      ...data,
      witness: null,
      updatedAt: firebaseApp.firestore.FieldValue.serverTimestamp(),
    })
    await batch.commit()
  }

  const makeUpdateDoc = (docId) => async (data) => {
    const docRef = entriesCollection.doc(docId)
    const batch = firestore.batch()
    batch.set(
      docRef,
      {
        ...data,
        updatedAt: firebaseApp.firestore.FieldValue.serverTimestamp(),
      },
      { merge: true }
    )
    batch.set(docRef.collection("dataCommit").doc(), {
      ...data,
      witness: null,
      updatedAt: firebaseApp.firestore.FieldValue.serverTimestamp(),
    })
    await batch.commit()
  }

  return { insertDoc, makeUpdateDoc }
}

export default useWriteJournal
