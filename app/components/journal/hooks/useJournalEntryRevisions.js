import React from "react"
import auth, { firebaseApp } from "../../auth/firebase-auth"
const firestore = firebaseApp.firestore()
import { useAuthState } from "react-firebase-hooks/auth"
import usePaginatedCollection from "../../common/hooks/usePaginatedCollection"

function useJournalEntryRevisions(docId) {
  const [{ uid }] = useAuthState(auth)
  const collection = firestore
    .collection(`users/${uid}/entries`)
    .doc(docId)
    .collection("dataCommit")
  return usePaginatedCollection(collection, "updatedAt", {
    orderByDirection: "desc",
    limit: 100,
  })
}

export default useJournalEntryRevisions
