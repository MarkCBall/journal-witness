import React from "react"
import { Table } from "semantic-ui-react"
import useJournalEntryRevisions from "./hooks/useJournalEntryRevisions"
import DateCell from "./column/DateCell"
import InputWithRevisions from "./column/InputWithRevisions"

function Row({ entry }) {
  const entryRevisions = useJournalEntryRevisions(entry.id)
  if (entryRevisions.error) {
    throw entryRevisions.error
  }

  return (
    <Table.Row>
      <DateCell entry={entry} />
      <InputWithRevisions
        field={"text1"}
        entry={entry}
        entryRevisions={entryRevisions}
      />
      <InputWithRevisions
        field={"text2"}
        entry={entry}
        entryRevisions={entryRevisions}
      />
      <InputWithRevisions
        field={"text3"}
        entry={entry}
        entryRevisions={entryRevisions}
      />
    </Table.Row>
  )
}

export default Row
