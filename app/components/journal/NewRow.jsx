import React, { useState } from "react"
import { Form, TextArea, Header, Button, Message } from "semantic-ui-react"
import useWriteJournal from "./hooks/useWriteJournal"
import { toast } from "react-toastify"
import { pickBy, identity } from "lodash"

function NewRow({ onClose }) {
  const [throwErr, setThrowErr] = useState(null)
  const { insertDoc } = useWriteJournal()
  const [data, setData] = useState({})
  const makeSetData = (field) => (e) =>
    setData((data) => ({ ...data, [field]: e.target.value }))

  if (throwErr) {
    setThrowErr(() => {
      toast.error("Failed to save change", {
        position: "bottom-center",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
      })
      return null
    })
  }

  const onSubmit = () =>
    insertDoc(pickBy(data, identity)).then(onClose).catch(setThrowErr)

  return (
    <Form style={{ padding: "40px" }}>
      <Header textAlign="center" color={"blue"} as={"h2"}>
        Add a journal entry
      </Header>
      <Header>First note</Header>
      <TextArea value={data.text1} onChange={makeSetData("text1")} />
      <Header>Second note</Header>
      <TextArea value={data.text2} onChange={makeSetData("text2")} />
      <Header>Third note</Header>
      <TextArea value={data.text3} onChange={makeSetData("text3")} />
      <div style={{ display: "flex", paddingTop: "20px" }}>
        <Message style={{ marginRight: "10%" }}>
          <Message.Header>You can edit these later</Message.Header>
          <p>
            What you write will be periodically witnessed on the blockchain so
            you can prove without a doubt that you did not make up your entries
            after the fact!
          </p>
        </Message>
        <div style={{ float: "right", verticalAlign: "bottom" }}>
          <Button size={"huge"} color={"blue"} onClick={onSubmit}>
            Add new entry
          </Button>
        </div>
      </div>
    </Form>
  )
}
export default NewRow
