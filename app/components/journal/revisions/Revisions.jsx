import React, { useState } from "react"
import { Popup, Icon, Accordion, Header, Loader } from "semantic-ui-react"
import styled from "styled-components"
import RevisionTitle from "./RevisionTitle"
import RevisionDetails from "./RevisionDetails"

const Clickable = styled.div`
  padding-left: 10px;
  cursor: pointer;
`
const PopupSize = styled.div`
  width: 450px;
  max-height: 300px;
  overflow-y: auto;
  padding: 5px;
`

function Revisions({ revisionData }) {
  const [activeIndex, setActiveIndex] = useState(-1)
  const handleOnClick = (_, titleProps) => {
    const clickedIndex = titleProps.index
    const newIndex = activeIndex === clickedIndex ? -1 : clickedIndex
    setActiveIndex(newIndex)
  }
  const content = (
    <PopupSize>
      <Header textAlign={"center"} as="h3">
        Revision history
      </Header>
      <Accordion fluid styled>
        {revisionData?.map((revision, index) => {
          const isActive = activeIndex === index
          const isWitnessed = !!revision.txId
          return (
            <div key={index}>
              <Accordion.Title
                active={isActive}
                index={index}
                onClick={handleOnClick}
              >
                <RevisionTitle revision={revision} />
                {isWitnessed ? (
                  <Icon name="dropdown" />
                ) : (
                  <>
                    <Loader active inline size={"mini"} />
                    Witnessing on blockchain. This may take 5 minutes.
                  </>
                )}
              </Accordion.Title>
              {isWitnessed && (
                <>
                  <Accordion.Content active={isActive}>
                    <RevisionDetails revision={revision} />
                  </Accordion.Content>
                </>
              )}
            </div>
          )
        })}
      </Accordion>
    </PopupSize>
  )

  const trigger = (
    <Clickable>
      <Popup
        trigger={
          <div>
            {revisionData.length}
            <Icon name={"archive"} />
          </div>
        }
        content={`View revision history`}
        inverted
        size={"mini"}
      />
    </Clickable>
  )

  return <Popup content={content} on="click" trigger={trigger} />
}
export default Revisions
