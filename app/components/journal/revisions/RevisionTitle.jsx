import React from "react"
import TimeAgo from "timeago-react"
import styled from "styled-components"

const RightJustified = styled.div`
  float: right;
  padding-bottom: 4px;
`

function RevisionTitle({ revision }) {
  const { updatedAt, text } = revision
  return (
    <div>
      <RightJustified>
        Edited: <TimeAgo datetime={updatedAt.toDate()} locale="en-US" />
      </RightJustified>
      {text}
    </div>
  )
}

export default RevisionTitle
