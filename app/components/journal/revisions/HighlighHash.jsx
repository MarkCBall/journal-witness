import React from "react"
import styled from "styled-components"

const Wrap = styled.div`
  overflow-wrap: anywhere;
`

function HighlightHash({ allHashes, hash }) {
  const [pre, post] = allHashes.split(hash)
  return (
    <Wrap>
      {pre}
      <b>{hash}</b>
      {post}
    </Wrap>
  )
}
export default HighlightHash
