import React from "react"
import { hashFunc } from "@journal-witness/libraries-crypto/hash"
import { Table, Icon, Popup } from "semantic-ui-react"
import moment from "moment"
import styled from "styled-components"
import HighlightHash from "./HighlighHash"

const MiniHash = styled.div`
  text-align: right;
  font-size: 10px;
`

function RevisionDetails({ revision }) {
  const { updatedAt, txId, text, hashInputs } = revision
  const hashedText = hashFunc(text)

  return (
    <Table basic={"very"} celled collapsing>
      <Table.Body>
        <Table.Row>
          <Table.Cell>Revision time</Table.Cell>
          <Table.Cell>
            {moment(updatedAt.toDate()).format("YYYY, MMM Do, HH:mm")}
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Text</Table.Cell>
          <Table.Cell>
            <a
              href={`https://md5calc.com/hash/sha256/${text}`}
              target={"_blank"}
            >
              {text}
            </a>
            <MiniHash>
              Sha256 hash: {hashFunc(text)}
              <Popup
                trigger={<Icon name={"help"} />}
                content={`This is hashed in "Witnessed" hashes" below`}
                inverted
                size={"mini"}
              />
            </MiniHash>
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Witnessed hashes</Table.Cell>
          <Table.Cell>
            <a
              href={`https://md5calc.com/hash/sha256/${hashInputs}`}
              target={"_blank"}
            >
              <HighlightHash allHashes={hashInputs} hash={hashedText} />
            </a>
            <MiniHash>
              Sha256 hash: {hashFunc(hashInputs)}
              <Popup
                trigger={<Icon name={"help"} />}
                content={`This is posted on the blockchain as the to address`}
                inverted
                size={"mini"}
              />
            </MiniHash>
          </Table.Cell>
        </Table.Row>

        <Table.Row>
          <Table.Cell>Blockchain witness transaction</Table.Cell>
          <Table.Cell>
            <a
              href={`https://ropsten.etherscan.io/tx/${txId}`}
              target={"_blank"}
            >
              {txId?.slice(0, 40)}...
            </a>
          </Table.Cell>
        </Table.Row>
      </Table.Body>
    </Table>
  )
}

export default RevisionDetails
