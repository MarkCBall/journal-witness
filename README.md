## Journal witness

This demo application allows users to create journal entries which are automatically witnessed on the blockchain. Currently ropsten ethereum test nextwork. It is not opinionated on what or how data is witnessed and can be configured as to how often data is witnessed.

A batch job is run to collect all un-witnessed text, and hash it together to minimize the number of blockchain transactions. From the single hash periodically posted to the blockchain, there is certainty that the text in the entry existed before that date. Using realtime firestore updates, when your data changes or is witnessed, you will know instantly.

With this app, you can easily prove the hash of the pdf of your homework existed before the due date, or just paste the whole text in!

### Inspiration

I believe there is a lot of value to be unlocked in proving provenance on the blockchain. Also this project is a good playground for many personal firsts:

- Used a scaling container as a service
- Managed the CD part of CICD
- Worked with realtime frontend data
- Meaningfully worked with a NoSQL database
- Locally emulated a database

### Tech Stack

- Yarn workspaces and a monorepo setup
- NextJS frontend
- Firebase authentication
- Firestore database
- Firebase Cloud functions
- GitLab runners for test reports in merge process
- Cloud build and cloud run for deployment and hosting

### Deployment notes

1. Start a firebase project/database

   - Note the client credentials in `Project settings/firebaseConfig` for the frontend .env
   - The URL the website is hosted on will need to be whitelisted
   - Enable the Google sign in method

2. Host the app

   - Push your code to Cloud source repositories or ensure it is synced to there
   - Add a Cloud build trigger watching for cloud source repository changes
   - Add the appropriate front end .env variables to the trigger
   - Set up a Cloud run instance

3. Backend as a service
   - Deploy through `cd functions && yarn deploy`
     - Webpack (workaround) is used to hoist monorepo libraries into the functions folder as firebase deploy all files from the root onwards
   - Ensure functions, indexes and rules are deployed
   - Put your ethereum private key into Google KMS
   - Ensure the Cloud functions have permissions to the secrets
4. Deploy locally
   - app
     - docker-compose build (generally only needed once)
     - docker compose up (volumes will inject/overwrite the files you are working on)
   - firebase
     - `cd firebase && firebase emulators:start`
     - note the cron job will need to be triggered manually through a test
     - .envrc
       - /.envrc helps instruct docker compose to run with user permissions instead of sudo
       - /firebase/.envrc sets the emulator host

### Housekeeping

1. Testing

   - You can run unit tests by running `yarn test:unit`
   - Integration tests are run via `yarn test:integration`
     - this will spin up the firebase emulators

2. Alerting/Logging
   - todo
