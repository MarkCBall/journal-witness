const path = require("path")
const CopyPlugin = require("copy-webpack-plugin")

// This is a monorepo workaround -- Deploy copies all files from the root folder up
// so use webpack to put the monorepo libraries into the same folder as functions
// https://github.com/firebase/firebase-tools/issues/653

module.exports = {
  target: "node",
  mode: "production",
  entry: "./functions/index.js",
  resolve: {
    extensions: [".js"],
  },
  output: {
    filename: "index.js",
    path: path.resolve(__dirname, "deploy/dist"),
    libraryTarget: "commonjs",
  },
  externals: {
    "firebase-admin": "firebase-admin",
    "firebase-functions": "firebase-functions",
    "@google-cloud/secret-manager": "@google-cloud/secret-manager",
    ethers: "ethers",
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "functions/package.json", to: "package.json" },
        { from: "firestore.rules", to: "firestore.rules" },
        { from: "firestore.indexes.json", to: "firestore.indexes.json" },
        // {to({context, absoluteFilename}){
        //   return `xx/${path.relative(context,absoluteFilename)}`
        //   }, from:"."}
        { from: ".firebaserc", to: ".." },
      ],
    }),
  ],
}
