### This folder is used as a staging area for deployment of firebase functions.

This is a monorepo workaround -- Staging files needed to deploy functions are copied/transpiled
into here via webpack. The firebase.json directs the deployment:

- `dist/` folder
- `.firebaserc` file

This workaround allows deploying functions from the functions folder and not the root folder
(containing all shared monorepo code) because the required monorepo libraries are transpiled in.
