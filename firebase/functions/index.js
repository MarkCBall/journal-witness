const fetchUnwitnessed = require("./witness/fetchUnwitnessed")
const hashUnwitnessed = require("./witness/hashUnwitnessed")
const witnessHash = require("./witness/witnessHash")
const saveHash = require("./witness/saveHash")
const functions = require("firebase-functions")

exports.witnessUnwitnessed = functions
  .runWith({ timeoutSeconds: 300 })
  .pubsub.schedule("every 5 minutes")
  .onRun(async (context) => {
    const unwitnessedDocSnapshots = await fetchUnwitnessed()
    const unwitnessedData = unwitnessedDocSnapshots.map((docSnapshot) =>
      docSnapshot.data()
    )
    const unwitnessedDocRefs = unwitnessedDocSnapshots.map(
      (snapshot) => snapshot.ref
    )
    if (unwitnessedDocSnapshots.length) {
      const { hashInputs, hash } = await hashUnwitnessed(unwitnessedData)
      //saving the hash could be added to pubsub for resiliency
      const txId = await witnessHash(hash)
      await saveHash(txId, unwitnessedDocRefs, hashInputs, hash)
    } else {
      console.log("Did not need to witness any entries.")
    }
    return null
  })
