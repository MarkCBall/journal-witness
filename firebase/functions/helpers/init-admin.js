const admin = require("firebase-admin")
!admin.apps.length && admin.initializeApp()
module.exports = admin
