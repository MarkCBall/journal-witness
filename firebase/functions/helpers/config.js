module.exports = {
  projectId: "famous-muse-303104",
  secretNames: {
    ropstenPrivKey: { key: "ropsten-priv-key", v: 2 },
    etherscanApi: { key: "etherscan-api", v: 1 },
  },
}
