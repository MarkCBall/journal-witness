import { PubSub } from "@google-cloud/pubsub"
import {
  createRevisedUnwitnessedEntry,
  createWitnessedEntry,
} from "../testHelpers/addJournalEntries"
const {
  initializeAdminApp,
  initializeTestApp,
  clearFirestoreData,
  apps,
} = require("@firebase/rules-unit-testing")
const projectId = "famous-muse-303104" //must match .firebaserc
const mockUid = "myuid"
const admin = initializeAdminApp({ projectId })
const client = initializeTestApp({ projectId, auth: { uid: mockUid } })
const adminFirestore = admin.firestore()
const clientFireStore = client.firestore()
const entriesCollection = adminFirestore
  .collection("users")
  .doc(mockUid)
  .collection("entries")

const resetData = async () => {
  clearFirestoreData({ projectId })
  await createWitnessedEntry(entriesCollection, admin)
  await createRevisedUnwitnessedEntry(entriesCollection, admin)
}
const closeLooseConnections = () =>
  Promise.all(apps().map((app) => app.delete()))

describe("firebase", () => {
  beforeEach(resetData)
  afterAll(closeLooseConnections)
  describe("authentication", () => {
    beforeEach(resetData)
    const otherUid = "otherUid"
    it("should allow write access to your own entries", async () => {
      const updateSelfDataReq = clientFireStore
        .collection("users")
        .doc(mockUid)
        .set({ data: "scrap" })
      return expect(updateSelfDataReq).resolves
    })
    it("shouldn't allow write access to other's entries", async () => {
      const updateOtherDataReq = clientFireStore
        .collection("users")
        .doc(otherUid)
        .set({ data: "scrap" })
      return expect(updateOtherDataReq).rejects.toThrow()
    })
    it("should should allow read access to anyone's entries", async () => {
      const scrapObject = { field: "scrapText" }
      await adminFirestore.collection(`users`).doc(otherUid).set(scrapObject)
      const docRef = await clientFireStore.collection(`users`).doc(otherUid)
      const documentData = (await docRef.get()).data()
      expect(documentData).toStrictEqual(scrapObject)
    })
    it("should should allow read access to nested entries", async () => {
      const scrapObject = { field: "scrapText" }
      const entryId = "x89"
      await adminFirestore
        .collection(`users/${otherUid}/entries`)
        .doc(entryId)
        .set(scrapObject)
      const docRef = await clientFireStore
        .collection(`users/${otherUid}/entries`)
        .doc(entryId)
      const documentData = (await docRef.get()).data()
      expect(documentData).toStrictEqual(scrapObject)
    })
  })
  describe(" batch hashing/witnessing", () => {
    it("should create a hash when the cron runs", async () => {
      expect(
        (await adminFirestore.collection("hashes").get()).docs.length
      ).toBe(0)
      const SCHEDULED_FUNCTION_TOPIC = "firebase-schedule-witnessUnwitnessed"
      const pubsub = new PubSub({
        projectId,
        apiEndpoint: "localhost:8085",
      })
      await pubsub.topic(SCHEDULED_FUNCTION_TOPIC).publishJSON({})
      // pubsub doesn't return a promise for when the task completes, so await an arbitrary length of time
      await new Promise((res) => setTimeout(res, 2000))
      expect(
        (await adminFirestore.collection("hashes").get()).docs.length
      ).toBe(1)
    })
  })
})
