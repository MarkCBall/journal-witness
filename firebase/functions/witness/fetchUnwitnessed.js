const admin = require("../helpers/init-admin")

const fetchUnwitnessed = async () => {
  const unwitnessedSnapshots = await admin
    .firestore()
    .collectionGroup("dataCommit")
    .where("witness", "==", null)
    .get()
  const unwitnessedDocSnapshots = unwitnessedSnapshots.docs
  return unwitnessedDocSnapshots
}

module.exports = fetchUnwitnessed
