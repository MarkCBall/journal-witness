const admin = require("../helpers/init-admin")

const fetchUnwitnessed = async (txId, unwitnessedDocRefs, hashInputs, hash) => {
  await admin.firestore().collection("hashes").doc(txId).set({
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    hashInputs,
    hash,
    txId,
  })
  unwitnessedDocRefs.forEach((docRef) =>
    docRef.set(
      {
        witness: admin.firestore.FieldValue.serverTimestamp(),
        hashInputs,
        txId,
      },
      { merge: true }
    )
  )
}

module.exports = fetchUnwitnessed
