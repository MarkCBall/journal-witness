const { Wallet, ethers } = require("ethers")
const { SecretManagerServiceClient } = require("@google-cloud/secret-manager")
const config = require("../helpers/config")

const witnessHash = async (data) => {
  //don't post to blockchain for testing
  if (process.env.FUNCTIONS_EMULATOR) {
    return "Mock-tx" + Math.random()
  }
  const client = new SecretManagerServiceClient()
  const [privKey] = await client.accessSecretVersion({
    name: `projects/${config.projectId}/secrets/${config.secretNames.ropstenPrivKey.key}/versions/${config.secretNames.ropstenPrivKey.v}`,
  })
  const [etherApi] = await client.accessSecretVersion({
    name: `projects/${config.projectId}/secrets/${config.secretNames.etherscanApi.key}/versions/${config.secretNames.etherscanApi.v}`,
  })
  const ethersPrivKey = privKey.payload.data.toString("utf-8")
  const etherscanApi = etherApi.payload.data.toString("utf-8")

  const provider = ethers.getDefaultProvider("ropsten", {
    etherscan: etherscanApi,
  })
  const wallet = new Wallet(ethersPrivKey, provider)
  const transaction = {
    //hacky way of storing a hash on the blockchain (in an empty address)
    to: `0x${data.slice(-40)}`,
    value: ethers.utils.parseEther("0"),
  }
  const createReceipt = await wallet.sendTransaction(transaction)
  await createReceipt.wait()
  return createReceipt.hash
}

module.exports = witnessHash
