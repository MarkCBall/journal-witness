const omit = require("lodash").omit
const flatten = require("lodash").flatten
const { hashFunc } = require("@journal-witness/libraries-crypto")

const hashUnwitnessed = async (unwitnessed) => {
  const strings = flatten(
    unwitnessed.map((rec) => Object.values(omit(rec, ["witness", "updatedAt"])))
  )
  const hashInputs = strings.map(hashFunc).join("_")
  const hash = hashFunc(hashInputs)
  return { hashInputs, hash }
}

module.exports = hashUnwitnessed
