//https://stackoverflow.com/questions/50411719/shared-utils-functions-for-testing-with-jest/52910794
// might scale better for shared test helpers
const admin = require("firebase-admin")

export const createWitnessedEntry = async (collection) => {
  await collection.doc("docId1").set({
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    id: "docId1",
    text1: "UnrevisedUnwitnessedEntry",
    updatedAt: admin.firestore.FieldValue.serverTimestamp(),
  })
  await collection
    .doc("docId1")
    .collection("dataCommit")
    .doc("values")
    .set({ text1: "UnrevisedUnwitnessedEntry", witness: "stuff" })
}

export const createRevisedUnwitnessedEntry = async (collection) => {
  await collection.doc("docId2").set({
    createdAt: admin.firestore.FieldValue.serverTimestamp(),
    id: "docId1",
    text1: "RevisedUnwitnessedEntry revision",
    updatedAt: admin.firestore.FieldValue.serverTimestamp(),
  })
  await collection
    .doc("docId2")
    .collection("dataCommit")
    .doc("values")
    .set({ text1: "RevisedUnwitnessedEntry initial", witness: null })
  await collection
    .doc("docId2")
    .collection("dataCommit")
    .doc("revisionId1")
    .set({
      text1: "RevisedUnwitnessedEntry revision",
      witness: null,
      updatedAt: admin.firestore.FieldValue.serverTimestamp(),
    })
}
