const crypto = require("crypto")
const hashFunc = (str) =>
  crypto.createHash("sha256").update(str).digest("hex").slice(-40)

module.exports.hashFunc = hashFunc

