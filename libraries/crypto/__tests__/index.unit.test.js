const { hashFunc } = require("../hash")

describe("crypto library", () => {
  it("should return a hash of 40 chars length", () => {
    const input = "any"
    expect(hashFunc(input).length).toEqual(40)
  })
})
